<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
    
    <documentation>
        <title>Fluid Mechanic</title>
        <authors>
            <author>ftoth</author>
        </authors>
        <date>2019-10-03</date>
        <keywords>
            <keyword>flow</keyword>
        </keywords>
        <references></references>
        <isVerified>yes</isVerified>
        <description>
Plane wave propagation in 3D in an arbitrary direction defined by k=[kx,ky,kz].
The mesh consits of a square channel extruded in k-direction from the yz-plane.
We use one element per cross section and, sufficient elements in extrusion (wave) direction 
to resolve the wave length.

The wave is exited by a displacement BC at the "input" surface. It is damped due to
bulk viscosity before it reaches the "out" surface. Plane waves develop, because only
tangential displacement is allowed on the "side" walls (normalSurfaceMass-BC with high factor).

The shear viscosity is arbitrary. If youset it too high you need to increase the "penalty" constraint
of the normalSurfaceMass.

The results agree with the 2D version "PlaneWave2DBulkViscosity".
        </description>
    </documentation>
    <fileFormats>
        <input>
            <!--<gmsh fileName="channel.msh"/>-->
            <hdf5 fileName="PlaneWave3DBulkViscosity.h5ref"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/><!-- material database -->
    </fileFormats>
    
    <domain geometryType="3d">
        <variableList>
            <var name="v" value="1"/>
            <var name="kx" value="1"/>
            <var name="ky" value="1/10"/>
            <var name="kz" value="1/3"/>
            <var name="k" value="sqrt(kx*kx+ky*ky+kz*kz)"/>
        </variableList>
        <regionList>
            <region name="V" material="FluidMat"/>
        </regionList> 
    </domain>
    
    <fePolynomialList>
        
        <Legendre id="velPolyId">
            <isoOrder>2</isoOrder> 
        </Legendre>
        
        <Legendre id="presPolyId">
            <isoOrder>1</isoOrder> 
        </Legendre>

    </fePolynomialList>

    
    <sequenceStep index="1"> 
        <analysis> 
            <harmonic>
                <frequencyList>
                    <freq value="10e+3"/>
                </frequencyList>
            </harmonic>
        </analysis>
        
        <pdeList>
            <fluidMechLin formulation="compressible" presPolyId="presPolyId" velPolyId="velPolyId">
                <regionList>
                    <region name="V"/>
                </regionList>
                
                <bcsAndLoads>
                    <velocity name="S_in">
                        <comp dof="x" value="v*kx/k"/>
                        <comp dof="y" value="v*ky/k"/>
                        <comp dof="z" value="v*kz/k"/>
                    </velocity>
                    <normalSurfaceMass name="S_side" value="1e5" volumeRegion="V"/>
                </bcsAndLoads>
                
                <storeResults >
                    <nodeResult  type="fluidMechPressure">
                        <allRegions/>   
                    </nodeResult>
                    <nodeResult type="fluidMechVelocity">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </fluidMechLin>             
        </pdeList>
        <!-- use direct LU since it is available in all builds, switch to e.g. PRADISO for a higher performance -->
        <linearSystems>
            <system>
                <solverList>
                    <directLU/>
                </solverList>
            </system>
        </linearSystems>         
    </sequenceStep>
    
    
</cfsSimulation>
