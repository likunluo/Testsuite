<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
    <documentation>
        <title>Test example for heat conduction on nonmatching grids.</title>
        <authors>
            <author>Simon Triebenbacher</author>
        </authors>
        <date>2011-07-27</date>
        <keywords>
            <keyword>mortar fem</keyword>
            <keyword>nonmatching grids</keyword>
            <keyword>lagrange multiplier</keyword>
        </keywords>
        <references>
            none
        </references>
        <isVerified> yes </isVerified>
        <description>
            This example served as a test case while implementing the Mortar FEM
            for the heat conduction PDE. The model consists of an inner part with
            a "heating coil" made from iron and a quadrilateral-shaped region of
            air surrounding it. Both regions are meshed with bilinear quadrilaterals
            The outer region consists of a disc meshed with linear triangles. The
            ratio of mesh sizes along the nonmatching interface is about 3:1.
            
            The mesh for the inner region was generated using the structured mesh
            generator inside cplreader:
            
            cplreader --type GENGRIDS --name heat_inner --xmlfile heat_inner_cplreader.xml
            
            The mesh for the outer region has been modelled using Gmesh 2.5 and can
            be generated using:
            
            gmsh -2 -bin -nopopup heat_outer.geo
            cfstool -m convert heat_outer.msh heat_outer.h5
            
            The current model serves also as an example for Helmut Koeck's project
            for KAI project concerning heating in packaged silicon chips.
        </description>
    </documentation>
    
    
    <fileFormats>
        <input>
            <hdf5 fileName="heat_inner.h5" id="inner"/>
            <hdf5 fileName="heat_outer.h5" id="outer"/>
        </input>
        
        <output>
            <hdf5/>
        </output>
        <materialData file="mat.xml"/>
    </fileFormats>
    
    <domain geometryType="plane">
        <regionList>
            <region name="coil" material="Iron"/>
            <region name="heat_inner" material="Air"/>
            <region name="outer" material="Air"/>
        </regionList>
        
        <surfRegionList>
            <surfRegion name="lower_inner"/>
            <surfRegion name="right_inner"/>
            <surfRegion name="upper_inner"/>
            <surfRegion name="left_inner"/>
            <surfRegion name="lower_outer"/>
            <surfRegion name="right_outer"/>
            <surfRegion name="upper_outer"/>
            <surfRegion name="left_outer"/>
        </surfRegionList>
        
        <ncInterfaceList>
            <ncInterface name="nciface_lower"
                masterSide="lower_inner"
                slaveSide="lower_outer"/>
            <ncInterface name="nciface_right"
                masterSide="right_inner"
                slaveSide="right_outer"/>
            <ncInterface name="nciface_upper"
                masterSide="upper_inner"
                slaveSide="upper_outer"/>
            <ncInterface name="nciface_left"
                masterSide="left_inner"
                slaveSide="left_outer"/>
        </ncInterfaceList>
        
        <nodeList>
            <nodes name="excite_one">
                <list>
                    <freeCoord comp="x" start="3.2" stop="6.8" inc="0.1"/>
                    <freeCoord comp="y" start="3.2" stop="3.8" inc="0.1"/>
                </list>
            </nodes>
            <nodes name="excite_two">
                <list>
                    <freeCoord comp="x" start="8.2" stop="11.8" inc="0.1"/>
                    <freeCoord comp="y" start="3.2" stop="3.8" inc="0.1"/>
                </list>
            </nodes>            
        </nodeList>
    </domain>
    
    <fePolynomialList>
        <Lagrange>
            <gridOrder/>
        </Lagrange>
    </fePolynomialList>
    
    <sequenceStep>
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <heatConduction>
                <regionList>
                    <region name="coil"/>
                    <region name="heat_inner"/>
                    <region name="outer"/>
                </regionList>
                
                <ncInterfaceList>
                    <ncInterface name="nciface_lower" formulation="Mortar"/>
                    <ncInterface name="nciface_right" formulation="Mortar"/>
                    <ncInterface name="nciface_upper" formulation="Mortar"/>
                    <ncInterface name="nciface_left" formulation="Mortar"/>
                </ncInterfaceList>
                
                <bcsAndLoads>
                    <temperature name="excite_one" value="1.0"/>
                    <temperature name="excite_two" value="-1.0"/>
                </bcsAndLoads>
                
                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </heatConduction>
            
        </pdeList>
        
        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <matrix storage="sparseNonSym"/>
                    </standard>
                </solutionStrategy>
                <solverList>
                    <pardiso>
                        <posDef>no</posDef>
                        <symStruct>no</symStruct>
                        <IterRefineSteps>100</IterRefineSteps>
                    </pardiso>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>


</cfsSimulation>
